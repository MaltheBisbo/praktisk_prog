#include <stdio.h>
#include <tgmath.h>
#include <limits.h>

#ifndef MY_KOMPLEX
#define MY_KOMPLEX

// Exercise 1 function declarations

long int getmax_while();
long int getmax_dowhile();
long int getmax_for();

long int getmin_while();
long int getmin_dowhile();
long int getmin_for();

typedef enum {w, dw, f} loop;
long double getstep_f(loop l);
long double getstep_d(loop l);
long double getstep_ld(loop l);

// Exercise 2 function declarations

float sum_up_float();
float sum_down_float();

double sum_up_double();
double sum_down_double();

// Exercise 3 function declarations

int equal(double a, double b, double tau, double epsilon);

#endif
