#include <stdio.h>
#include <tgmath.h>
#include <limits.h>
#include "header.h"



//*** FUNCTIONS EXERCISE 1 ***//



/********* Maximum values *********/

long int getmax_while(){
int i=1;
while(i+1>i){
	i++;
}
return i;
}

long int getmax_dowhile(){
int i=1;
do{
	i++;
}
while(i+1>i);
return i;
}

long int getmax_for(){
int num=1;
for(int i=1; i+1>i; i++){
	num++;
}
return num;
}

/********* Minimum values *********/

long int getmin_while(){
int i=1;
while(i+1>i){
	i++;
}
return i;
}

long int getmin_dowhile(){
int i=1;
do{
	i++;
}
while(i+1>i);
return i;
}

long int getmin_for(){
int num=1;
for(int i=1; i+1>i; i++){
	num++;
}
return num;
}



long double getstep_f(loop l){
float x = 1;
switch(l){
case 0:
	while(x+1 != 1){
		x /= 2;
	}
	x *= 2;
case 1:
	do{
		x/=2;
	} while(x+1 != 1);
	x *= 2;
case 2:
	for(x = 1; x+1 != 1; x /= 2){}
	x*=2;
}
return x;
}

long double getstep_d(loop l){
double x = 1;
switch(l){
case 0:
	while(x+1 != 1){
		x /= 2;
	}
	x *= 2;
case 1:
	do{
		x/=2;
	} while(x+1 != 1);
	x *= 2;
case 2:
	for(x = 1; x+1 != 1; x /= 2){}
	x*=2;
}
return x;
}

long double getstep_ld(loop l){
long double x = 1;
switch(l){
case 0:
	while(x+1 != 1){
		x /= 2;
	}
	x *= 2;
case 1:
	do{
		x/=2;
	} while(x+1 != 1);
	x *= 2;
case 2:
	for(x = 1; x+1 != 1; x /= 2){}
	x*=2;
}
return x;
}






//*** FUNCTIONS EXERCISE 2 ***//



float sum_up_float(){
float k = 1;
float x = 0;
int max = INT_MAX/2;
for(int i = 1; i <= max; i++){
	x += k/i;
}
return x;
}


float sum_down_float(){
float k = 1;
float x = 0;
int max = INT_MAX/2;
for(int i = max; i >= 1; i--){
	x += k/i;
}
return x;
}

double sum_up_double(){
double k=1;
double x = 0;
int max = INT_MAX/2;
for(int i = 1; i <= max; i++){
x += k/i;
}
return x;
}

double sum_down_double(){
double k = 1;
double x = 0;
int max = INT_MAX/2;
for(int i = max; i >= 1; i--){
	x += k/i;
}
return x;
}
