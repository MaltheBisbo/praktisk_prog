#include <tgmath.h>
#include <stdlib.h>
#include "header.h"

int equal(double a, double b, double tau, double epsilon){
if( ( fabs(a-b) < tau) || (fabs(a-b)/(fabs(a)+fabs(b)) < epsilon) ){
	return 1;
} else{
	return 0;
}
}
