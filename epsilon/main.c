#include <stdio.h>
#include <tgmath.h>
#include <limits.h>
#include "header.h"

int main(){
//*** EXERCISE 1 ***//
//**************  Største og mindste integer ****************

// Max
long int diff_while = getmax_while() - INT_MAX;
printf("myMax - INT_MAX = %li - %i = %li\n", getmax_while(), INT_MAX, diff_while);

long int diff_dowhile = getmax_while() - INT_MAX;
printf("myMax - INT_MAX = %li - %i = %li\n", getmax_while(), INT_MAX, diff_dowhile);

long int diff_for = getmax_while() - INT_MAX;
printf("myMax - INT_MAX = %li - %i = %li\n", getmax_while(), INT_MAX, diff_for);

// Min
long int diff_while_min = getmax_while() - INT_MAX;
printf("myMin - INT_MIN = %li%+i = %li\n", getmin_while(), INT_MIN, diff_while_min);

long int diff_dowhile_min = getmax_while() - INT_MAX;
printf("myMin - INT_MIN = %li%+i = %li\n", getmin_while(), INT_MIN, 		diff_dowhile_min);

long int diff_for_min = getmax_while() - INT_MAX;
printf("myMin - INT_MIN = %li%+i = %li\n", getmin_while(), INT_MIN, diff_for_min);

//********   mindste "step" for float, double og long double ********

// Float
//loop xw = w; loop xdw = dw; loop xf
printf("Float:\n");
printf("Using while    : myDiff = %.20Lg\n", getstep_f(w));
printf("Using do-while : myDiff = %.20Lg\n", getstep_f(dw));
printf("Using for      : myDiff = %.20Lg\n", getstep_f(f));

// Double
//dblType xd = d;
printf("Double:\n");
printf("Using while    : myDiff = %.20Lg\n", getstep_d(w));
printf("Using do-while : myDiff = %.20Lg\n", getstep_d(dw));
printf("Using for      : myDiff = %.20Lg\n", getstep_d(f));

// Long Double
//dblType xld = ld;
printf("Long double:\n");
printf("Using while    : myDiff = %.20Lg\n", getstep_ld(w));
printf("Using do-while : myDiff = %.20Lg\n", getstep_ld(dw));
printf("Using for      : myDiff = %.20Lg\n", getstep_ld(f));

//*** EXERCISE 2 ***//

double xf = sum_up_float();
printf("Sum up float : xf = %lf\n",xf);

double xd = sum_up_double();
printf("Sum up double : xd = %lg\n",xd);

double xf_down = sum_down_float();
printf("Sum down float : xf = %lf\n",xf_down);

double xd_down = sum_down_double();
printf("Sum down double : xd = %lg\n",xd_down);

printf("The sum of 1/n is divergent.\n");


//*** EXERCISE 3 ***//

// Check if numbers are equal
double a = 3.111;
double b = 3.342;
double tau = 0.1;
double epsilon = 0.1;
int eq = equal(a,b,tau,epsilon);
printf("equal? : %i\n",eq);

return 0;
}
