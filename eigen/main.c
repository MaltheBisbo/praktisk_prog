#include <stdio.h>
#include <tgmath.h>
#include <stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_eigen.h>
#include <gsl/gsl_linalg.h>

#define pi 3.1415

int main() {
//************ PART 1 ************//

// We considder the system of linear equations given in the problem
// and describe it by Ax=b.

//define the matrix A
{
double A_data[] =   {6.13, -2.90, 5.86,
                     8.08, -6.31, -3.89,
                     -4.36, 1.00, 0.19};

double b_data[] = {6.23, 5.37, 2.29};

gsl_matrix_view A = gsl_matrix_view_array(A_data,3,3);
gsl_vector_view b = gsl_vector_view_array(b_data,3);

gsl_vector *x = gsl_vector_alloc(3);

int s;
gsl_permutation * p = gsl_permutation_alloc (3);
gsl_linalg_LU_decomp (&A.matrix, p, &s);
gsl_linalg_LU_solve (&A.matrix, p, &b.vector, x);

double x_vec[3];
printf("Part 1\n");
printf("x = [  ");
for(int i = 0; i<3; i++){
  double xi = gsl_vector_get(x,i);
  x_vec[i] = xi;
  printf("%g  ", x_vec[i]);
}
printf("]\n");

printf("check of solution:\n");
double b1 = 6.13*x_vec[0] - 2.90*x_vec[1] + 5.86*x_vec[2];
double b2 = 8.08*x_vec[0] - 6.31*x_vec[1] - 3.89*x_vec[2];
double b3 = -4.36*x_vec[0] + 1.00*x_vec[1] + 0.19*x_vec[2];
printf("b_check = [%g  %g  %g]\n", b1, b2, b3);

gsl_permutation_free (p);
gsl_vector_free (x);
}
//************ PART 2 ************//
FILE* mystream = fopen("data.dat","w");
{
  int n = 100;
  double s = 1/(double)(n+1);
  gsl_matrix *H = gsl_matrix_calloc(n,n);
  for(int i=0;i<n-1;i++){
    gsl_matrix_set(H,i,i,-2); // set diagonal elements except last
    gsl_matrix_set(H,i,i+1,1); // set elements below diagonal
    gsl_matrix_set(H,i+1,i,1); // set elements above diagonal
  }
  gsl_matrix_set(H,n-1,n-1,-2); // set last diagonal element
  gsl_matrix_scale(H,-1/s/s); // scale by 1/s²

  gsl_eigen_symmv_workspace* w =  gsl_eigen_symmv_alloc (n); // create vector
                              // used during calculations in gsl_eigen_symmv
  gsl_vector* eval = gsl_vector_alloc(n); // allocate vector for eigenvalues
  gsl_matrix* evec = gsl_matrix_calloc(n,n); // allocate matrix for eigenvectors
  gsl_eigen_symmv(H,eval,evec,w); // calculate eigenvalues and eigenvectors of H



  gsl_eigen_symmv_sort(eval,evec,GSL_EIGEN_SORT_VAL_ASC); // sort eigenvalues
                                        // and eigenvectors in ascenting order
  printf("Part 2\n");
  printf("The eigenvalues are:\n");
  fprintf (stderr, "i   exact   calculated\n");
  for (int k=0; k < 10; k++){
    double exact = pi*pi*(k+1)*(k+1);
    double calculated = gsl_vector_get(eval,k);
    fprintf (stderr, "%i   %g   %g\n", k, exact, calculated);
  }



  for(int i = 0; i < n; i++) {
    double eigenvalue = gsl_vector_get(eval, i);
    printf("%g\n",eigenvalue);
  }

fprintf(mystream, "x\t");
for(int i=0;i<7;i++){
  fprintf(mystream, "v_%d\t", i);
}
fprintf(mystream,"\n");
for(int k = 0; k<8; k++) fprintf(mystream, "%g\t",0.0);
fprintf(mystream, "\n");
for(int i = 0; i < n; i++) {
  fprintf(mystream, "%g\t", (i+1.0)/(n+1));
  for(int j = 0; j<7; j++){
    double eigenvector = gsl_matrix_get(evec, i, j);
    fprintf(mystream,"%+5.2e\t",eigenvector);
  }
fprintf(mystream,"\n");
}
fprintf(mystream, "%g\t",1.0);
for(int k = 0; k<7; k++) fprintf(mystream, "%g\t",0.0);
}

//************ PART 3 *************//
// something is wrong here
/*
{

int n=1500;
double s= 1.0/(n-1);

gsl_matrix *H = gsl_matrix_calloc(n,n);
for(int i = 0; i<n-1; i++){ // 1 to n-1
  gsl_matrix_set(H,i,i,-2);
  gsl_matrix_set(H,i,i+1,1);
  gsl_matrix_set(H,i+1,i,1);
}
gsl_matrix_set(H,n-1,n-1,-2);
//gsl_matrix_set(H,n-2,n-2,-2);
//gsl_matrix_set(H,0,1,1);
//gsl_matrix_set(H,n-1,n-2,1);
gsl_matrix_scale(H,-1.0/s*s);

gsl_matrix *H2 = gsl_matrix_calloc(n,n);
for(int i = 0; i<n; i++){
  for(int j = 0; j<n; j++){
    gsl_matrix_set(H2,i,j,(i-(n-1)/2)*(i-(n-1)/2)*s*s);
  }
}
gsl_matrix_add(H,H2);

gsl_eigen_symmv_workspace* work =  gsl_eigen_symmv_alloc (n); // create vector
                            // used during calculations in gsl_eigen_symmv
gsl_vector* eval = gsl_vector_alloc(n); // allocate vector for eigenvalues
gsl_matrix* evec = gsl_matrix_calloc(n,n); // allocate matrix for eigenvectors
gsl_eigen_symmv(H,eval,evec,work); // calculate eigenvalues and eigenvectors of H

gsl_eigen_symmv_sort(eval,evec,GSL_EIGEN_SORT_VAL_ASC);

fprintf (stderr, "\n\ni   exact   calculated\n");
for (int k=0; k < 10; k++){
  double exact = 2*k+1;
  double calculated = gsl_vector_get(eval,k);
  fprintf (stderr, "%i   %g   %g\n", k, exact, calculated);
}

fprintf(mystream,"\n\n\n");
fprintf(mystream, "x\t");
for(int i=0;i<7;i++){
  fprintf(mystream, "v_%d\t", i);
}
//for(int k = 0; k<4; k++) fprintf(mystream, "%g\t",0.0);
fprintf(mystream, "\n");
for(int i = 0; i < n; i++) {
  fprintf(mystream, "%g\t", (i+1.0)/(n+1));
  for(int j = 0; j<3; j++){
    double eigenvector = gsl_matrix_get(evec, i, j);
    fprintf(mystream,"%+5.2e\t",eigenvector);
  }
fprintf(mystream,"\n");
}
fprintf(mystream, "%g\t",1.0);
//for(int k = 0; k<3; k++) fprintf(mystream, "%g\t",0.0);

printf("\n\n");
for(int i = 0; i<n; i++){
  printf("%d\t%g\n", i, (i-(n-1)*1.0/2)*(i-(n-1)*1.0/2)*s*s);
}

}
*/



  return 0;
}
