#ifndef MY_KOMPLEX
#define MY_KOMPLEX

typedef struct {double re,im;} komplex;

void    komplex_set      (komplex* z, double x, double y);  /* z ← x+iy */
komplex komplex_new      (double x , double y );            /* returns x+iy */
komplex komplex_add      (komplex a, komplex b);            /* returns a+b */
komplex komplex_sub      (komplex a, komplex b);            /* returns a-b */
komplex komplex_conjugate(komplex z);                 /* returns complex conjugate */

int     komplex_equal    (komplex a, komplex b, double tau, double eps);  
/* returns 1 if equal, 0 otherwise */
void	komplex_print    (char* s,komplex z);               /* prints s z */
komplex komplex_mul      (komplex a, komplex b);            /* returns a*b */
komplex komplex_div      (komplex a, komplex b);            /* returns a/b */
komplex komplex_exp      (komplex z); /* returns complex exponential function */
komplex komplex_sin      (komplex z); /* returns complex sin  */
komplex komplex_cos      (komplex z); /* returns complex cos  */
komplex komplex_sqrt     (komplex z);
komplex komplex_abs      (komplex z);

#endif
