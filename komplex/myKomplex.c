#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "myKomplex.h"
#include <string.h>


void komplex_set(komplex* z, double x, double y){
	(*z).re = x;
	(*z).im = y;
};

komplex komplex_new(double x , double y ){
	komplex z = {.re = x, .im = y};
	return z;
}

komplex komplex_add      (komplex a, komplex b){
	komplex z ={.re = a.re + b.re, .im = a.im + b.im};
	return z;
}

komplex komplex_sub      (komplex a, komplex b){
	komplex z ={.re = a.re - b.re, .im = a.im - b.im};
	return z;
}

komplex komplex_conjugate(komplex z){
	komplex conj = {.re = z.re, .im = -z.im};
	return conj;
}

int komplex_equal(komplex a, komplex b, double tau, double eps){
	if( ( (fabs(a.re-b.re) < tau) && (fabs(a.im-b.im) < tau) ) || \
	( (fabs(a.re-b.re)/	(fabs(a.re)+fabs(b.re)) < eps) && \
	(fabs(a.im-b.im)/(fabs(a.im)+fabs(b.im)) < eps) )){
		return 1;
	} else{
		return 0;
	}
}

void komplex_print(char* s,komplex z){
	printf("%c = %g%+gi\n", *s, z.re, z.im);
}

komplex komplex_mul(komplex a, komplex b){
	komplex z;
	z.re = a.re*b.re - a.im*b.im;
	z.im = a.re*b.im + a.im*b.re;
	return z;
}

komplex komplex_div      (komplex a, komplex b){
	//komplex z;
	//komplex temp = komplex_mul(a,komplex_conjugate(b));
	
	//z.re = temp.re/(double)komplex_mul(b,komplex_conjugate(b));
	//z.im = temp.im/(double)komplex_mul(b,komplex_conjugate(b));
	//return z;
}

komplex komplex_exp      (komplex z){

}

komplex komplex_sin      (komplex z){

}

komplex komplex_cos      (komplex z){

}

komplex komplex_sqrt     (komplex z){

}

komplex komplex_abs      (komplex z){

}






















