#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "myKomplex.h"
#include <string.h>


int main(){
// komplex_set
double x = 1.5;
double y = -2.4;
komplex z;
komplex_set(&z,x,y);
printf("The complex number is: %g%+gi\n", z.re, z.im);

// komplex_new
komplex z_new = komplex_new(x,y);
printf("The new complex number is: %g%+gi\n", z_new.re, z_new.im);
komplex a = {.re = 1.1, .im = 2.2};
komplex b = {.re = 1.1, .im = 2.2};

// komplex_sub
komplex z_add = komplex_sub(a,b);
printf("The sum of the complex numbers is: %g%+gi\n", z_add.re, z_add.im);

// komplex_equal
double tau = pow(10,-6)
double eps = pow(10,-6);
int equal = komplex_equal(a, b, tau, eps);
printf("Are they equal: %i\n", equal);

// komplex_print
char s = 's';
komplex_print(&s,z);

// komplex_mul
komplex z_mul = komplex_mul(a,b);
printf("a*b = %g%+gi\n", z_mul.re, z_mul.im);

// komplex_div
komplex z_div = komplex_div(a,b);
printf("a/b = %g%+gi\n", z_div.re, z_div.im);

return 0;
}
