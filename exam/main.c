#include<stdio.h>
#include<getopt.h>
#include<math.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>


double mysqrt(double xval);

int main(){

  printf("x\tmysqrt(x)\tsqrt(x)\n");
  int Npoints = 100;
  double xmin=0.1, xmax=5;
  double dx = (xmax-xmin)/Npoints;
  for(int i=0; i<Npoints; i++){
    double xi = xmin+i*dx;
    printf("%g\t%g\t%g\n", xi, mysqrt(xi), sqrt(xi));
  }

  return 0;
}
