#include<math.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>

int sqrt_diff (double x, const double y[], double dydt[], void * params) {
  dydt[0] = y[0]/(2*x);
  return GSL_SUCCESS;
}

double mysqrt(double xval){
  double xeval;
  if(xval<1){
    xeval=1/xval;
  } else {
    xeval=xval;
  }
  gsl_odeiv2_system sys;
  sys.function = sqrt_diff;
  sys.jacobian = NULL;
  sys.dimension = 1;
  sys.params = NULL;

  double hstart = 1e-6;
  double epsabs = 1e-6;
  double epsrel = 1e-6;
  gsl_odeiv2_driver * driver =
  gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_rkf45,hstart, epsabs, epsrel);

  double y[1] = {1}; // Known point
  double x = 1;      // Known point
  double x1 = xeval;  // x-value for which we want solution
  int status = gsl_odeiv2_driver_apply (driver, &x, x1, y);
  if (status != GSL_SUCCESS){
    printf ("error, return value=%d\n", status);
  }

  gsl_odeiv2_driver_free (driver);

  if(xval<1){
    return 1/y[0];
  } else{
    return y[0];
  }

}
