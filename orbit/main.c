#include<stdio.h>
#include<getopt.h>
#include<math.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>

#define M_PI 3.1415

//*********** PART 1 functions ************//

int diff_eq (double t, const double y[], double dydt[], void * params) {
  dydt[0] = y[0]*(1-y[0]);
  return GSL_SUCCESS;
}

// Logistic function: f(x) = L/(1+exp(-k(x-x0)))
void logistic_func(double L, double x0, double k, double x, double *y){
  *y = L/(1+exp(-k*(x-x0)));
}

//*********** PART 2 functions ************//

// System of differential equations for equitorial motion
// arround star in general relativity.
int diff_orbit (double t, const double y[], double dydt[], void * params) {
  //(void)(t); /* avoid unused parameter warning */
  double epsilon = *(double *)params;
  dydt[0] = y[1];
  dydt[1] = 1-y[0]+epsilon*y[0]*y[0];
  return GSL_SUCCESS;
}

//*********** PART 3 functions ************//

typedef struct {
  double m;
  double E;
} mystruct;

double Vr(double r) {
  double Vr = -50*exp(-pow(r/1.7,2));
  return Vr;
}

int diff_PS(double r, const double u[], double dudr[], void *params) {
  mystruct par = *(mystruct*)params;
  double m = par.m;
  double E = par.E;
  dudr[0] = u[1];
  dudr[1] = 2*m/pow(197.3,2)*Vr(r)*u[0] - 2*m/pow(197.3,2)*E*u[0];
  return GSL_SUCCESS;
}



int main() {

//*************** Part 1 ****************//
{
  gsl_odeiv2_system sys;
  sys.function = diff_eq;
  sys.jacobian = NULL;
  sys.dimension = 1;
  sys.params = NULL;

  double hstart = 1e-6;
  double epsabs = 1e-6;
  double epsrel = 1e-6;
  gsl_odeiv2_driver * driver =
  gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_rkf45,hstart, epsabs, epsrel);

  double y[1] = {0.5};
  double t = 0.0;
  double t1 = 3.0;
  for(int i = 0; i<100; i++){
    double ti = i*t1/100.0;
    int status = gsl_odeiv2_driver_apply (driver, &t, ti, y);
    if (status != GSL_SUCCESS){
      printf ("error, return value=%d\n", status);
      break;
    }
    printf("%0.5e\t%0.5e\n",t,y[0]);

  }
  printf("\n\n");
  double y_teo;
  for(int i = 0; i<100; i++){
    double ti = i*t1/100.0;
    logistic_func(1,0,1,ti,&y_teo);
    printf("%0.5e\t%0.5e\n", ti, y_teo);
  }

  gsl_odeiv2_driver_free (driver);
}
//*************** Part 2 ****************//

for(int j = 0; j<3; j++){
printf("\n\n");
double epsilon[3] = {0.0 , 0.0 , 0.01};
double y0[3] = {1.0 , 1.0 , 1.0};
double dy0dt[3] = {0.0 , 0.5 , 0.5};
double phi_max[3] = {10.0 , 10.0 , 133.7};
double phi_min[3] = {0.0 , 0.0 , 0.0};

gsl_odeiv2_system sys_orbit;
sys_orbit.function = diff_orbit;
sys_orbit.jacobian = NULL;
sys_orbit.dimension = 2;
sys_orbit.params = &epsilon[j];

double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
gsl_odeiv2_driver * driver_orbit =
gsl_odeiv2_driver_alloc_y_new (&sys_orbit, gsl_odeiv2_step_rk8pd,hstart, epsabs, epsrel);

double y[2] = {y0[j] , dy0dt[j]};
double delta_phi = 0.01;
double phi = phi_min[j];
printf("phi\tu\n");
for(double phii = phi_min[j]; phii<phi_max[j]; phii+=delta_phi){
  int status = gsl_odeiv2_driver_apply (driver_orbit, &phi, phii, y);
  if (status != GSL_SUCCESS){
    printf ("error, return value=%d\n", status);
    break;
  }
  printf("%0.5e\t%0.5e\n",phi,y[0]);

}
gsl_odeiv2_driver_free(driver_orbit);
}



//*************** Part 3 ****************//
  {
  FILE* mystream = fopen("data.txt","w");
  printf("\n\n");
  fprintf(mystream,"E\tdelta\n");
  double delta;
  double k;
  for(int E = 1; E<101; E++){
  double m = 940;
  //int E = 1;
  mystruct params = {m,(double)E};
  gsl_odeiv2_system sys_PS = {&diff_PS, NULL, 2, &params};

  double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
  gsl_odeiv2_driver * driver_PS =
  gsl_odeiv2_driver_alloc_y_new (&sys_PS, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel);

  double u0 = 0, du0dr = 1;
  double u[2] = {u0 , du0dr};
  double r_min = 0, r_max = 10, dr = 0.1;
  double r = r_min;
  printf("r\tu\n");
  for(double ri = r_min; ri<r_max; ri+=dr){
    int status = gsl_odeiv2_driver_apply (driver_PS, &r, ri, u);
    if (status != GSL_SUCCESS){
      printf ("error, return value=%d\n", status);
      break;
    }
    if(E==10) printf("%0.5e\t%0.5e\n",r,u[0]);
  }
  gsl_odeiv2_driver_free(driver_PS);
  k = sqrt(2*m*E)/197.3;
  delta = atan(k*u[0]/u[1])-k*r_max;
  fprintf(mystream,"%d\t%g\n",E,delta);
  }
  }

  return 0;
}
