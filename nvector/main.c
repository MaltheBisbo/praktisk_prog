#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "nvector.h"
#include <assert.h>

int main() {
  int n = 2;
  nvector* vec = nvector_alloc(n);
  for(int i=0; i<vec -> size ; i++){
  nvector_set(vec, i, i+1);
  }
  printf("vec[1] = %g\n",nvector_get(vec,1));

  printf("vec.vec = %g\n",nvector_dot_product(vec,vec));
  char s[]= "The vector is:";
  nvector_print(s, vec);
  nvector_print("The vector is:", vec);
  nvector_add(vec,vec);
  nvector_print("The vector is:", vec);
  nvector_scale(vec,10);
  nvector_print("The vector is:", vec);
  nvector_free(vec);
  return 0;
}
