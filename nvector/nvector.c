#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "nvector.h"
#include <assert.h>

nvector* nvector_alloc       (int n) {
  nvector* vec = malloc(sizeof(nvector));
  vec -> size = n;
  vec -> data = malloc(n*sizeof(double));
  if (vec==NULL) printf("\n there is an error with the vector \n");
  return vec;
}

void     nvector_free        (nvector* v) {free(v -> data); free(v);}

void     nvector_set         (nvector* v, int i, double value){
  assert(0<= i && i < v->size);
  v -> data[i] = value;
}

double   nvector_get         (nvector* v, int i) {
  assert(0<= i && i < v->size);
  return v -> data[i];
}

double   nvector_dot_product (nvector* u, nvector* v){
  double dot;
  if (u -> size == v -> size){
  for (int i = 0; i < v -> size;i++){
    dot += (v -> data[i])*(u -> data[i]);
  }
}else printf("The vectors don't have the same size");
  return dot;
}
void nvector_print    (char* s, nvector* v){
  printf("%s",s);
  printf(" [");
  for(int i=0; i<v->size-1;i++) {printf("%g,",v->data[i]);}
  printf("%g]\n",v->data[v->size-1]);
}

void nvector_set_zero (nvector* v){
  for (int i=0; i<v->size;i++) {v->data[i]=0;};
}

int  nvector_equal    (nvector* u, nvector* v){
  int equal = 1;
  if (u -> size == v -> size){
  for (int i = 0; i < v -> size;i++){
    if(u->data[i] == v->data[i]){
    } else {equal = 0; break;}
  }
  }else printf("The vectors don't have the same size");
  return equal;
}

void nvector_add      (nvector* u, nvector* v){
  if (u -> size == v -> size){
  for (int i = 0; i < v -> size;i++){
    u->data[i] += v->data[i];
  }
  }else printf("The vectors don't have the same size");
}

void nvector_sub      (nvector* u, nvector* v){
  if (u -> size == v -> size){
  for (int i = 0; i < v -> size;i++){
    u->data[i] -= v->data[i];
  }
  }else printf("The vectors don't have the same size");
}

void nvector_scale      (nvector* u, double x){
  for (int i = 0; i < u -> size;i++){
    u->data[i] *= x;
  }
}
