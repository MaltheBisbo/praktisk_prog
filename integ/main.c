#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>

double f1 (double x, void *params){
  double f = log(x)/sqrt(x);
  return f;
}

double f21 (double x, void *params){
  double alpha = *(double*)params;
  double f = exp(-alpha*x*x);
  return f;
}

double f22 (double x, void *params){
  double alpha = *(double*)params;
  double f = (-alpha*alpha*x*x/2 + alpha/2 + x*x/2)*exp(-alpha*x*x);
  return f;
}

int main(){
//*********** PART 1 ************//
{
  gsl_integration_workspace *w = gsl_integration_workspace_alloc(1000);

  gsl_function F;
  F.function = &f1;
  F.params = NULL;

  double result;
  double error;

  double epsabs = 1e-7;
  double epsrel = 1e-7;

  double a = 0;
  double b = 1;

  gsl_integration_qags (&F, a, b, epsabs, epsrel, 1000, w, &result, &error);

  printf("The integral gives: %g\n",result);
  gsl_integration_workspace_free(w);
}
  //*********** PART 2 ************//
{
  gsl_integration_workspace *w = gsl_integration_workspace_alloc(1000);

  gsl_function F1;
  F1.function = &f21;

  gsl_function F2;
  F2.function = &f22;

  double result1;
  double error1;

  double result2;
  double error2;

  double epsabs = 1e-7;
  double epsrel = 1e-7;
double alpha0 = 0;
double alpha1 = 2;
int Nsteps = 100;
double alpha;
double E;
double d_alpha = (alpha1-alpha0)/Nsteps;
double E_min = 100;
double alpha_min;
FILE *mystream = fopen("data.dat","w");
fprintf(mystream,"alpha\tE\n");
for(int i = 1; i < Nsteps; i++){
  alpha = d_alpha*i;
  F1.params = &alpha;
  F2.params = &alpha;
  gsl_integration_qagi(&F2,epsabs,epsrel,1000,w,&result2,&error2);
  gsl_integration_qagi(&F1,epsabs,epsrel,1000,w,&result1,&error1);
  E = result2/result1;
  fprintf(mystream,"%g\t%g\n", alpha, E);
  if(E<E_min) {
    E_min = E;
    alpha_min = alpha;
  }
}
printf("The value of alpha that minimizes the energy is: %g\n", alpha_min);
printf("where the energy is %g\n", E_min);
gsl_integration_workspace_free(w);
}


  return 0;
}
