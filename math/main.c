#include <stdio.h>
#include <tgmath.h>

# define M_E  2.7182818284590452354 /* e */
# define M_PI 3.1415926535897932384 /* pi */

int main(){
// part 1
	double x = 5;
	double gam5 = tgamma(x);
	printf("tgamma(5) = %f\n",gam5);

	double x1 = 0.5;
	double Bessel = j1(x1);
	printf("Bessel1(0.5) = %g\n",Bessel);

	double x2 = -2;
	double _Complex sq = csqrt(x2);
	printf("sqrt(-2) = %g + i%g\n",creal(sq), cimag(sq));

	_Complex x3 = I;
	double _Complex expi = cexp(x3);
	printf("exp(i) = %g + i%g\n",creal(expi), cimag(expi));

	_Complex x4 = M_PI*I;
	double _Complex expip = cexp(x4);
	printf("exp(i*pi) = %g + i%lg\n",creal(expip), cimag(expip));

	double _Complex ie = cpow(I,exp(1));
	printf("i^e = %lg + %lgi\n",creal(ie) ,cimag(ie));

// Part 2

	printf("\nPart 2\n");

	float f = 0.1111111111111111111111111111111;
	double d = 0.1111111111111111111111111111111;
	long double ld = 0.1111111111111111111111111111111L;
	printf("f  = %.25g\n",f);
	printf("d  = %.25lg\n",d);
	printf("ld = %.25Lg\n",ld);

	return 0;
}
